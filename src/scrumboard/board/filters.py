from django.contrib.auth import get_user_model

import django_filters

from .models import Sprint, Task

User = get_user_model()


class Nullfilter(django_filters.BooleanFilter):
    """
    Filter on a field set as null or not.
    """
    def filter(self, qs, value):
        if value is not None:
            return qs.filter(**{'{0}__isnull'.format(self.name): value})


class TaskFilter(django_filters.FilterSet):
    backlog = Nullfilter(name='sprint')

    class Meta:
        model = Task
        fields = ('sprint', 'status', 'assigned', 'backlog',)

    def __init__(self, *args, **kwargs):
        super(TaskFilter, self).__init__(*args, **kwargs)
        self.filters['assigned'].extra.update(
            {'to_field_name': User.USERNAME_FIELD})


class SprintFilter(django_filters.FilterSet):
    end_min = django_filters.DateFilter(name='end', lookup_type='gte')
    end_max = django_filters.DateFilter(name='end', lookup_type='lte')

    class Meta:
        model = Sprint
        fields = ('end_min', 'end_max',)
