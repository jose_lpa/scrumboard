Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows:

WSGIDaemonProcess scrumboard-<target> threads=5 maximum-requests=1000 user=<user> group=staff
WSGIRestrictStdout Off

<VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/scrumboard/log/apache2/error.log"
        CustomLog "/srv/sites/scrumboard/log/apache2/access.log" common

        WSGIProcessGroup scrumboard-<target>

        Alias /media "/srv/sites/scrumboard/media/"
        Alias /static "/srv/sites/scrumboard/static/"

        WSGIScriptAlias / "/srv/sites/scrumboard/src/teamq/wsgi/wsgi_<target>.py"

</VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

[program:uwsgi-scrumboard-<target>]
user = <user>
command = /srv/sites/scrumboard/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/scrumboard/src/scrumboard/wsgi/wsgi_<target>.py
home = /srv/sites/scrumboard/env
master = true
processes = 8
harakiri = 600
autostart = true
autorestart = true
stderr_logfile = /srv/sites/scrumboard/log/uwsgi_err.log
stdout_logfile = /srv/sites/scrumboard/log/uwsgi_out.log
stopsignal = QUIT

Nginx
-----

upstream django_scrumboard_<target> {
  ip_hash;
  server 127.0.0.1:8001;
}

server {
  listen :80;
  server_name  my.domain.name;

  access_log /srv/sites/scrumboard/log/nginx-access.log;
  error_log /srv/sites/scrumboard/log/nginx-error.log;

  location /500.html {
    root /srv/sites/scrumboard/src/scrumboard/templates/;
  }
  error_page 500 502 503 504 /500.html;

  location /static/ {
    alias /srv/sites/scrumboard/static/;
    expires 30d;
  }

  location /media/ {
    alias /srv/sites/scrumboard/media/;
    expires 30d;
  }

  location / {
    uwsgi_pass django_scrumboard_<target>;
  }
}
