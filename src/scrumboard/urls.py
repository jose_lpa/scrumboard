from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView

from rest_framework.authtoken.views import obtain_auth_token

from scrumboard.board.urls import router


urlpatterns = patterns(
    '',
    url(r'api/token/', obtain_auth_token, name='api-token'),
    url(r'api/', include(router.urls)),
    url(r'^$', TemplateView.as_view(template_name='board/index.html')),
)

# NOTE: The staticfiles_urlpatterns also discovers static files (ie. no need to
# run collectstatic). Both the static folder and the media folder are only
# served via Django if DEBUG = True.
urlpatterns += staticfiles_urlpatterns() + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
